<?php

namespace Database\Factories;

use App\Models\Gericht;
use Illuminate\Database\Eloquent\Factories\Factory;

class GerichtFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Gericht::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
