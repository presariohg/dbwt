<!DOCTYPE html>
<html lang="de">
    <head>
        <title>Ihre E-Mensa</title>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="{{asset('css/styling.css')}}">

    </head>


    <body>

        <header id="header" class="header">
            <img src="logo.jpg" alt="Something logo something" id="img_logo">
            <div id="div_menubar">
                <ul>
                    <li>
                        <a href="/#textbox" title="">Ankündigung</a>
                    </li>
                    <li>
                        <a href="/#tbl_menu">Speisen</a>
                    </li>
                    <li>
                        <a href="/#div_payment">Zahlen</a>
                    </li>
                    <li>
                        <a href="/#contacts">Kontakt</a>
                    </li>
                    <li>
                        <a href="/#wichtig">Wichtig für uns</a>
                    </li>
                </ul>
            </div>
        </header>

        <div id="div_horizontal_grid_3">
            <div id="div_vertical_grid">
                @yield('content')
            </div>
        </div>

        <footer>
            <div>
                <ul>
                    <li>(c) E-Mensa GmbH</li>
                    <li>Tam Nguyen</li>
                    <li><a href="#impressum">Impressum</a></li>
                </ul>
            </div>
        </footer>

    </body>

</html>
