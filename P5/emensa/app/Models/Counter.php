<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Counter extends Model {
    protected $primaryKey = 'counter_name';
    protected $table = 'counter';
    protected $keyType = 'string';

    public $incrementing = false;
    public bool $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'counter_name',
        'value'
    ];

    use HasFactory;
}
