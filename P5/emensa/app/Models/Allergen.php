<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

//TODO: if need be, change this to enable adding more allergens
class Allergen extends Model {
    protected $table = 'allergen';
    protected $primaryKey = 'code';
    protected $keyType = 'string';

    public $incrementing = false;
    public bool $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'typ'
    ];

    use HasFactory;
}
