<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GerichtKategorie extends Model {
    protected $table = 'gericht_hat_kategorie';
    protected $primaryKey = ['gericht_id', 'kategorie_id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gericht_id',
        'kategorie_id'
    ];
    use HasFactory;
}
