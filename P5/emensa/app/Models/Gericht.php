<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Gericht extends Model {
    protected $primaryKey = 'id';
    protected $table = 'gericht';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'beschreibung',
        'vegan',
        'vegetarisch',
        'preis_intern',
        'preis_extern'
        ];

    const CREATED_AT = 'erfasst_am';
    use HasFactory;

    public function getAllergen(): BelongsToMany {
        return $this->belongsToMany(Allergen::class, 'gericht_hat_allergen', 'gericht_id', 'code');
    }
}
