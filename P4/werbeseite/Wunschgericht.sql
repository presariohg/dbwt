-- SET NAMES utf8mb4_unicode_ci
//Aufgabe 1_2
USE emensawerbeseite;

CREATE TABLE ersteller (
    erstellerID BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY UNIQUE ,
    email       VARCHAR(50) NOT NULL UNIQUE ,
    name        VARCHAR(20) DEFAULT 'anonym'
);

CREATE TABLE wunschgericht (
    id              BIGINT AUTO_INCREMENT PRIMARY KEY,
    gericht_name    VARCHAR(50) ,
    beschreibung    VARCHAR(100) ,
    datum           DATE DEFAULT CURRENT_DATE,
    erstellerID     BIGINT,
    FOREIGN KEY (erstellerID) REFERENCES ersteller(erstellerID)
)ENGINE=INNODB;


//ersteller 1_6
INSERT INTO ersteller(email, name) VALUES ('teddy@mr.bean', 'bean');
INSERT INTO ersteller(email) VALUES ('boho@yahoo.net');
INSERT INTO ersteller(email, name) VALUES ('saki@null.net', 'saki');
INSERT INTO ersteller(email, name) VALUES ('some@mail.org','somsom');

INSERT INTO ersteller(email, name)
    VALUES ('boho@yahoo.net', 'boho')
    ON DUPLICATE KEY UPDATE name = VALUES(name);

INSERT INTO ersteller(email)
    VALUES ('saki@null.net')
    ON DUPLICATE KEY UPDATE name = DEFAULT;

INSERT INTO ersteller(email)
    VALUES ('some@mail.org')
    ON DUPLICATE KEY UPDATE name = DEFAULT;

INSERT INTO wunschgericht (gericht_name, beschreibung, datum, erstellerID)
    SELECT 'Currywurst mit Pommes','Currywurst beschreibung', '2020-08-23',erstellerID FROM ersteller WHERE email = 'boho@yahoo.net';
INSERT INTO wunschgericht (gericht_name, beschreibung, datum, erstellerID)
    SELECT 'Hackbraten','Nicht nur für Hacker', '2020-08-25', erstellerID FROM ersteller WHERE email = 'some@mail.org';
INSERT INTO wunschgericht (gericht_name, beschreibung, erstellerID)
    SELECT 'Hühnersuppe','Suppenhuhn trifft Petersilie', erstellerID FROM ersteller WHERE email = 'boho@yahoo.net';
INSERT INTO wunschgericht (gericht_name, beschreibung, datum, erstellerID)
    SELECT 'Lasagne','Klassisch mit Bolognesesoße und Creme Fraiche', '2020-08-26', erstellerID FROM ersteller WHERE email = 'saki@null.net';
INSERT INTO wunschgericht (gericht_name, beschreibung,  erstellerID)
    SELECT 'Lasagne','Klassisch mit Bolognesesoße und Creme Fraiche', erstellerID FROM ersteller WHERE email = 'teddy@mr.bean';



SELECT * FROM ersteller;
SELECT * FROM wunschgericht;

SELECT ID, GERICHT_NAME, BESCHREIBUNG, DATUM, EMAIL, NAME FROM wunschgericht
    INNER JOIN ersteller ON wunschgericht.erstellerID = ersteller.erstellerID
    ORDER BY datum DESC LIMIT 5;







