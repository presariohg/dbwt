<!--
    Praktikum DBWT. Autoren:
    Nguyen, Duc Tam, 3233521
    Tran, Anh Minh, 3246773
 -->
<?php
require("model.php");


function valid_data(array $post):bool
{
    $ename = trim($post['ersteller']);
    $email = trim($post['email']);

    $ename = filter_var($ename, FILTER_SANITIZE_STRING);
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    //if name=""
    if ($ename == "")
        $_POST['ersteller'] = 'anonym';
    // break if invalid entry
    $is_valid = !((substr_count($email, '@') != 1) || (substr_count($email, '.') < 1));
    if (!$is_valid)
        return false;

    $invalid_emails = ["@trashmail", "@rcpt.at", "@damnthespam.at", "@wegwerfmail.at"];
    foreach ($invalid_emails as $invalid_word) {
        if (strpos($email, $invalid_word)) {
            $is_valid = false;
            break;
        }
    }
    if (!$is_valid)
        return false;
    return true;
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>wunschgericht</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="wunschgericht.css">
</head>
<body>
<h1>Ihr Wunschgericht zu erstellen</h1>
<div class="container">
    <form action="wunschgericht.php" method="post">
        <div class="row">
            <div class="col-25">
                <label for="gname">Gericht Name</label>
            </div>
            <div class="col-75">
                <input required type="text" id="gname" name="gname" placeholder="Name Ihres Gerichts.">
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label for="date">Datum</label>
            </div>
            <div class="col-75">
                <input required type="date" id="date" name="date" placeholder="Erstellsdatum..">
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label for="beschreibung">Beschreibung</label>
            </div>
            <div class="col-75">
                <textarea required id="beschreibung" name="beschreibung" placeholder="beschreiben Sie Ihr Gericht.." style="height:200px"></textarea>
            </div>
        </div>

        <div class="row">
            <div class="col-25">
                <label for="ersteller">Ersteller/in</label>
            </div>
            <div class="col-75">
                <input type="text" id="ersteller" name="ersteller" placeholder="Name des/der Erstellers/in">
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label for="email">E-mail</label>
            </div>
            <div class="col-75">
                <input required type="email" id="email" name="email" placeholder="admin@gmail.com">
            </div>
        </div>
        <div class="row">
            <input type="submit" name="submit" value="Submit">
        </div>
    </form>
</div>
<?php
$link = db_connect();
if ($link === false)
    die("ERROR: could not connect. ") . mysqli_connect_error();
if(isset($_POST['submit']) && valid_data($_POST)) {
    echo "data sind valid!<br>";
    //!$output = shell_exec($_POST['gname']);
    //echo "<pre>", $output, "</pre>";
    $gname = mysqli_real_escape_string($link, $_POST['gname']);

    $datum = mysqli_real_escape_string($link, $_POST['date']);

    $beschreibung = mysqli_real_escape_string($link, $_POST['beschreibung']);

    $ersteller = mysqli_real_escape_string($link, $_POST['ersteller']);

    $email = mysqli_real_escape_string($link, $_POST['email']);

    /*
    $sql = "INSERT INTO wunschgericht(gericht_name, datum, beschreibung, ersteller, email) VALUE ('$gname','$datum','$beschreibung','$ersteller','$email')";
    if (mysqli_query($link, $sql)) {
        echo "Records added successfully. ";
    } else {
        echo "ERROR:could not able to execute $sql." . mysqli_error($link);
    }
    */

    /**
     * Security: Statement vorbereiten, Data in Datenbank hinfügen
     */
    $statement_ersteller = mysqli_stmt_init($link);
    mysqli_stmt_prepare($statement_ersteller, "INSERT INTO ersteller(email, name) VALUES (?,?) ON DUPLICATE KEY UPDATE name = VALUES(name)");
    mysqli_stmt_bind_param($statement_ersteller, 'ss', $email, $ersteller);
    if (mysqli_stmt_execute($statement_ersteller)) {
        echo "Ersteller/in erfolgreich hinzugefügt." . '\n';
    } else {
        echo "ERROR:could not able to execute $statement_ersteller." . mysqli_error($link);
    }
    $id_ersteller = mysqli_insert_id($link);

    $statement_wunsch = mysqli_stmt_init($link);
    mysqli_stmt_prepare($statement_wunsch, "INSERT INTO wunschgericht(gericht_name, beschreibung, erstellerID)  SELECT ?,?, erstellerID FROM ersteller WHERE email = '$email'");
    mysqli_stmt_bind_param($statement_wunsch, 'ss', $gname, $beschreibung);
    if (mysqli_stmt_execute($statement_wunsch)) {
        echo "Ihr Wunschgericht erfolgreich hinzugefügt";
    } else {
        echo "ERROR:could not able to execute $statement_wunsch." . mysqli_error($link);
    }
    $id_wunsch = mysqli_insert_id($link);

}

else
    echo "data sind invalid. Bitte valid data einfügen!";
mysqli_close($link);
?>
</body>
</html>