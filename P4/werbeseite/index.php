<!-- 
    Praktikum DBWT. Autoren:
    Nguyen, Duc Tam, 3233521
    Tran, Anh Minh, 3246773
 -->

<?php
require("model.php");

$connection = db_connect();

increase_visit_counter($connection);
$is_valid_registration = true;
$has_request = isset($_POST['email']);


if ($has_request)
    $is_valid_registration = register_newsletter($_POST, $connection);

?>

<!DOCTYPE html>
<html lang="de">
    <head>
        <title>Ihre E-Mensa</title>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="styling.css">
    </head>


    <body>

        <header id="header" class="header">
            <img src="logo.jpg" alt="Something logo something" id="img_logo">
            <div id="div_menubar">
                <ul>
                    <li>
                        <a href="#textbox" title="">Ankündigung</a>
                    </li>
                    <li>
                        <a href="#tbl_menu">Speisen</a>
                    </li>
                    <li>
                        <a href="#div_payment">Zahlen</a>
                    </li>
                    <li>
                        <a href="#contacts">Kontakt</a>
                    </li>
                    <li>
                        <a href="#wichtig">Wichtig für uns</a>
                    </li>
                </ul>
            </div>
        </header>

        <div id="div_horizontal_grid_3">
            <div id="div_vertical_grid">
                <img src="banner.jpg" alt="Something banner something" id="img_banner">

                <h2>Bald gibt es Essen auch online ;-)</h2>
                <p id="textbox">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br>
                    Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt.
                </p>

                <h2>Köstlichkeiten, die Sie erwarten</h2>
                <table id = "tbl_menu">
                    <thead>
                        <tr>
                            <th>Bild</th>
                            <th>Speise</th>
                            <th>Preis intern</th>
                            <th>Preis extern</th>
                            <th>Allergene</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
//                            $meals = json_decode(file_get_contents('./meals.json'), true);
//                            foreach ($meals as $meal) {
//                                echo "<tr>";
//                                echo '<td align="center"><img src="'.$meal['img'].'"><img></td>';
//                                echo '<td>'.$meal['description']."</td>";
//                                echo '<td align="center">'.$meal['price_intern']."</td>";
//                                echo '<td align="center">'.$meal['price_extern']."</td>";
//                                echo "</tr>";
//                            }
                        $query = "SELECT name, preis_intern, preis_extern, id
                                    FROM gericht ORDER BY name LIMIT 5 ";

                        $result = mysqli_query($connection, $query);
                        if (!$result)
                            write_error_log(mysqli_error($connection));
                        else {
                            while ($row = mysqli_fetch_assoc($result)) {
                                echo "<tr>";
                                echo "<td>img placeholder</td>";
                                $gericht_id = "";
                                // print out beschreibungen & prices
                                foreach ($row as $key => $value) {
                                    if ($key == 'id') {
                                        $gericht_id = $value;
                                        continue;
                                    }
                                    echo "<td>".$value."</td>";
                                }

                                // find allergens of this dish
                                $query = "SELECT name FROM allergen
                                            JOIN gericht_hat_allergen gha ON allergen.code = gha.code
                                            WHERE gha.gericht_id = '${gericht_id}'";
                                $allergens = mysqli_query($connection, $query);
                                if (!$allergens)
                                    write_error_log($connection);
                                else {
                                    echo "<td>";
                                    while ($row = mysqli_fetch_assoc($allergens)) {
                                        foreach ($row as $allergen)
                                            echo $allergen.' ';
                                    }
                                    echo "</td>";
                                }

                                echo "</tr>";
                            }
                        }

                        ?>

                        <tr>
                            <td>...</td>
                            <td>...</td>
                            <td>...</td>
                            <td>...</td>
                            <td>...</td>
                        </tr>
                    </tbody>
                </table>

                <h2>E-Mensa in Zahlen</h2>

                <div id="div_mid_list">
                    <ol>
                        <?php
                            $visit_counter = get_counter_value($connection, "visits");
                            echo "<li>${visit_counter} Besuche</li>";

                            $newsletter_counter = get_counter_value($connection, "newsletter");
                            echo "<li>${newsletter_counter} Anmeldungen zum Newsletter</li>";

                            $meal_counter = get_counter_value($connection, "meals");
                            echo "<li>${meal_counter} Speisen</li>";
                        ?>
                    </ol>
                </div>

                <h2>Interesse geweckt? Wir informieren Sie!</h2>
                <form action="index.php" method="post" accept-charset="utf-8">
                    <fieldset id="fs_newsletter" >
                        <div id="div_vorname">
                            <label for="vorname">Ihr Name:</label>
                            <input required id="vorname" name="vorname" placeholder="Vorname">
                        </div>
                        <div id="div_email">
                            <label for="email">Ihre E-Mail:</label>
                            <input required id="email" name="email" placeholder="admin@example.com">
                        </div>
                        <div id="div_language">
                            <label for="language">Newsletter bitte in:</label>
                            <select id="language" name="language">
                                <option>Deutsch</option>
                                <option>Englisch</option>
                            </select>
                        </div>
                        <div id="div_confirm_read">
                            <input required id="confirm_read" type="checkbox" name="confirm_read">
                            <label for="confirm_read">Den Datenschutzbestimmungen stimme ich zu</label>
                        </div>
                        <div id="div_submit">
                            <input type="submit" name="btn_submit" value="Zum Newsletter anmelden">
                        </div>
                        <p>
                            <?php
                                if ($has_request)
                                    if ($is_valid_registration)
                                        echo "Erfolgreich gespeichert!";
                                    else
                                        echo "Ungültige Eingaben!";
                            ?>
                        </p>
                    </fieldset>
                </form>
                <h2 id="wichtig">Das ist uns wichtig</h2>            
                <div>
                    <ul>
                        <li>Beste frische saisonale Zutaten</li>
                        <li>Ausgewogene abwechslungsreiche Gerichte</li>
                        <li>Sauberkeit</li>
                    </ul>
                </div>
                <a id="wunschgericht" href="wunschgericht.php">Ihr Wunschgericht hier erstellen</a>
                <h2>Wir freuen uns auf Ihren Besuch!</h2>
            </div>
        </div>

    <footer>
        <div>
            <ul>
                <li>(c) E-Mensa GmbH</li>
                <li>Tam Nguyen</li>
                <li><a href="#impressum">Impressum</a></li>
            </ul>
        </div>
    </footer>

    </body>

</html>