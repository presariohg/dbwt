# 8a
create unique index idx_gericht_kategorie on gericht_hat_kategorie(kategorie_id, gericht_id);

# 8b
create unique index idx_name on gericht(name);

# 8c
alter table gericht_hat_kategorie
    drop foreign key gericht_hat_kategorie_ibfk_1;

alter table gericht_hat_kategorie
    add constraint fk_gericht_id
        foreign key (gericht_id) references gericht(id) on delete cascade ;

alter table gericht_hat_allergen
    add constraint  fk_gericht_id
        foreign key (gericht_id) references gericht(id) on delete cascade;

insert into gericht value (22, 'Suppenkreation', 'gut und günstig', '2020-08-27', 0, 0, 0.5, 0.9);
insert into gericht_hat_kategorie value (3, 22);

# 8d
alter table gericht_hat_kategorie
    add constraint fk_kategorie_id
        foreign key (kategorie_id) references kategorie(id) on delete no action ;

alter table kategorie
    add constraint fk_eltern_id
        foreign key (eltern_id) references kategorie(id) on delete no action;

insert into kategorie value (6, 1, 'Mensastars', 'kat_stars.tif');

# 8e
alter table gericht_hat_allergen
    add constraint fk_alergen_code
        foreign key (code) references allergen(code) on update cascade ;

# 8f
alter table gericht_hat_kategorie
    add primary key (gericht_id, kategorie_id);