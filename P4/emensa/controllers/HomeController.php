<?php
require_once('../models/gericht.php');
require_once("../models/helper.php");
require_once("../models/counter.php");

/* Datei: controllers/HomeController.php */
class HomeController {
    public function index(RequestData $request) {
        $connection = db_connect();

        increase_visit_counter($connection);
        $is_valid_registration = true;
        $has_request = isset($_POST['email']);

        if ($has_request)
            $is_valid_registration = register_newsletter($_POST, $connection);

        // get 5 gerichte from db
        $query = "SELECT name, preis_intern, preis_extern, id
                                    FROM gericht ORDER BY name LIMIT 5 ";

        $gerichte = [];
        $result = mysqli_query($connection, $query);
        if (!$result)
            write_error_log(mysqli_error($connection));
        else {
            while ($row = mysqli_fetch_assoc($result)) {
                $this_gericht = $row;
                $this_gericht['allergens'] = [];

                // find allergens of this dish
                $query = "SELECT name FROM allergen
                                JOIN gericht_hat_allergen gha ON allergen.code = gha.code
                                WHERE gha.gericht_id = '${this_gericht['id']}'";
                $allergen_rows = mysqli_query($connection, $query);
                if (!$allergen_rows)
                    write_error_log($connection);
                else {
                    while ($row = mysqli_fetch_assoc($allergen_rows)) {
                        foreach ($row as $allergen)
                            array_push($this_gericht['allergens'], $allergen);
                    }
                }
                array_push($gerichte, $this_gericht);
            }
        }

        // get counters from db
        $counters['visits'] = get_counter_value($connection, 'visits');
        $counters['newsletter'] = get_counter_value($connection, 'newsletter');
        $counters['meals'] = get_counter_value($connection, 'meals');

        return view('emensa.index', ['rq' => $request,
                                     'is_valid_registration' => $is_valid_registration,
                                     'has_request' => $has_request,
                                     'gerichte' => $gerichte,
                                     'counters' => $counters]);
    }

    public function empty_page(RequestData $request) {
        return view('emensa.empty_page', ['rq' => $request]);
    }
}