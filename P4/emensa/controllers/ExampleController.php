<?php
require_once('../models/kategorie.php');
require_once('../models/gericht.php');

class ExampleController {
    public function m4_a6(RequestData $requestData) {
        return view('home', ['rq' => $requestData]);
    }

    public function m4_6a_queryparameter(RequestData $rd) {
        if (isset($rd->query['name']))
            $result = "Der Wert von ?name lautet: &lt;{$rd->query['name']}&gt;";
        else
            $result = "?name nicht gefunden!";

        return view('examples.m4_6a_queryparameter', ['result' => $result]);
    }

    public function m4_6b_kategorie(RequestData $requestData) {
        $categories = db_kategorie_select_all();
        return view('examples.m4_6b_kategorie', ['categories' => $categories]);
    }

    public function m4_6c_gerichte(RequestData $requestData) {
        $dishes = db_gericht_6c();
        return view('examples.m4_6c_gerichte', ['dishes' => $dishes]);
    }

    public function m4_6d_layout(RequestData $requestData) {

        if ($requestData->query['no'] == '2')
            $page = 'm4_6d_page_2';
        else
            //default landing to page 1
            $page = 'm4_6d_page_1';

        return view('examples.pages.'.$page, []);
    }
}