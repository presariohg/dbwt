<?php
/**
 * Mapping of paths to controls.
 * Note, that the path only support 1 level of directory depth:
 *     /demo is ok,
 *     /demo/subpage will not work as expected
 */
return array(
    "/"            => "HomeController@index",
    "/empty"       => "HomeController@empty_page",
    "/demo"        => "DemoController@demo",
    '/dbconnect'   => 'DemoController@dbconnect',

    // Erstes Beispiel:
    "/m4_a6"                => "ExampleController@m4_a6",
    '/m4_6a_queryparameter' => 'ExampleController@m4_6a_queryparameter',
    '/m4_6b_kategorie'      => 'ExampleController@m4_6b_kategorie',
    '/m4_6c_gerichte'       => 'ExampleController@m4_6c_gerichte',
    '/m4_6d_layout'         => 'ExampleController@m4_6d_layout',
);