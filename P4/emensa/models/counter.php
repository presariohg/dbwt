<?php

/**
 * Get a counter value from db
 * @param mysqli $connection The connection being used to connect to db
 * @param string $counter_name The counter to be increased
 * @return int The counter's value
 */
function get_counter_value(mysqli $connection, string $counter_name) : int {
    switch ($counter_name) {
        case "visits":
            $query = "SELECT value FROM counter WHERE counter_name = '${counter_name}'";
            $result = mysqli_query($connection, $query);

            if ($result)
                return mysqli_fetch_assoc($result)['value'];
            else
                write_error_log(mysqli_error($connection));
            break;

        case "newsletter":
            $query = "SELECT COUNT(email) FROM newsletter";
            $result = mysqli_query($connection, $query);

            if ($result)
                return array_values(mysqli_fetch_assoc($result))[0];
            else
                write_error_log(mysqli_error($connection));
            break;

        case "meals":
            $query = "SELECT COUNT(id) FROM gericht";
            $result = mysqli_query($connection, $query);

            if ($result)
                return array_values(mysqli_fetch_assoc($result))[0];
            else
                write_error_log(mysqli_error($connection));
            break;
        default:
            return 0;
    }
}


/**
 * Increase a counter in db by one
 * @param mysqli $connection The connection being used to connect to db
 */
function increase_visit_counter(mysqli $connection) {
    $query = "SELECT value FROM counter WHERE counter_name = 'visits'";

    $result = mysqli_query($connection, $query);

    if ($result) {
        $counter_value = (int) mysqli_fetch_assoc($result)['value'];
        $counter_value++;

        $query = "UPDATE counter SET value = ${counter_value} WHERE counter_name = 'visits'";
        if (mysqli_query($connection, $query)) {
            return;
        } else {
            write_error_log(mysqli_error($connection));
        }
    } else {
        write_error_log(mysqli_error($connection));
    }
}


