<!-- 
    Praktikum DBWT. Autoren:
    Nguyen, Duc Tam, 3233521
    Tran, Anh Minh, 3246773
 -->

<?php

if (isset($_POST['email'])) {
    $email = trim($_POST['email']);
    $vorname = trim($_POST['vorname']);
    $language = $_POST['language'];

    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    $vorname = filter_var($vorname, FILTER_SANITIZE_STRING);

//    break if invalid entry
    $is_invalid = (($vorname == "") ||
                   (substr_count($email, '@') != 1) ||
                   (substr_count($email, '.') < 1));

    $invalid_emails = ["@trashmail", "@rcpt.at", "@damnthespam.at", "@wegwerfmail.at"];

    foreach ($invalid_emails as $invalid_word) {
        if (strpos($email, $invalid_word)) {
            $is_invalid = true;
            break;
        }
    }

    if (!$is_invalid) {
        $new_entry = [
            "email"     => $email,
            "vorname"   => $vorname,
            "language"  => $language];

//    read old email list
        $email_list = json_decode(file_get_contents('./data.json'), true) ?? [];

//    push new entry
        array_push($email_list, $new_entry);

//    write back down
        file_put_contents('./data.json', json_encode($email_list, JSON_PRETTY_PRINT));
    }
}

?>

<!DOCTYPE html>
<html lang="de">
    <head>
        <title>Ihre E-Mensa</title>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="styling.css">
    </head>


    <body>

        <header id="header" class="header">
            <img src="logo.jpg" alt="Something logo something" id="img_logo">
            <div id="div_menubar">
                <ul>
                    <li>
                        <a href="#textbox" title="">Ankündigung</a>
                    </li>
                    <li>
                        <a href="#tbl_menu">Speisen</a>
                    </li>
                    <li>
                        <a href="#div_payment">Zahlen</a>
                    </li>
                    <li>
                        <a href="#contacts">Kontakt</a>
                    </li>
                    <li>
                        <a href="#wichtig">Wichtig für uns</a>
                    </li>
                </ul>
            </div>
        </header><!-- /header -->

        <div id="div_horizontal_grid_3">
            <div id="div_vertical_grid">
                <img src="banner.jpg" alt="Something banner something" id="img_banner">

                <h2>Bald gibt es Essen auch online ;-)</h2>
                <p id="textbox">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br>
                    Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt.
                </p>

                <h2>Köstlichkeiten, die Sie erwarten</h2>
                <table id = "tbl_menu">
                    <thead>
                        <tr>
                            <th>Bild</th>
                            <th>Speise</th>
                            <th>Preis intern</th>
                            <th>Preis extern</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                            $meals = json_decode(file_get_contents('./meals.json'), true);
                            foreach ($meals as $meal) {
                                echo "<tr>";
                                echo '<td align="center"><img src="'.$meal['img'].'"><img></td>';
                                echo '<td>'.$meal['description']."</td>";
                                echo '<td align="center">'.$meal['price_intern']."</td>";
                                echo '<td align="center">'.$meal['price_extern']."</td>";
                                echo "</tr>";
                            }
                        ?>
<!--                        <tr>-->
<!--                            <td>Rindfleisch mit Bambus, Kaiserschoten und rotem Paprika, dazu Mie Nudeln</td>-->
<!--                            <td align="center">3,50</td>-->
<!--                            <td align="center">6,20</td>-->
<!--                        </tr>-->

<!--                        <tr>-->
<!--                            <td>Spinatrisotto mit kleinen Samosateigecken und gemischter Salat</td>-->
<!--                            <td align="center">2,90</td>-->
<!--                            <td align="center">5,30</td>-->
<!--                        </tr>-->

                        <tr>
                            <td align="center">...</td>
                            <td align="center">...</td>
                            <td align="center">...</td>
                            <td align="center">...</td>
                        </tr>
                    </tbody>
                </table>

                <h2>E-Mensa in Zahlen</h2>
                <div id="div_payment">
                   <p>Something placeholder something</p> 
                </div>

                <div id="div_mid_list">
                    <ol>
                        <li>Besuche</li>
                        <li>Anmeldungen zum Newsletter</li>
                        <li>Speisen</li>
                    </ol>
                </div>

                <h2>Interesse geweckt? Wir informieren Sie!</h2>
                <form action="index.php" method="post" accept-charset="utf-8">
                    <fieldset id="fs_newsletter" >
                        <div id="div_vorname">
                            <label for="vorname">Ihr Name:</label>
                            <input required id="vorname" name="vorname" placeholder="Vorname">
                        </div>
                        <div id="div_email">
                            <label for="email">Ihre E-Mail:</label>
                            <input required id="email" name="email" placeholder="admin@example.com">
                        </div>
                        <div id="div_language">
                            <label for="language">Newsletter bitte in:</label>
                            <select id="language" name="language">
                                <option>Deutsch</option>
                                <option>Englisch</option>
                            </select>
                        </div>
                        <div id="div_confirm_read">
                            <input required id="confirm_read" type="checkbox" name="confirm_read">
                            <label for="confirm_read">Den Datenschutzbestimmungen stimme ich zu</label>
                        </div>
                        <div id="div_submit">
                            <input type="submit" name="btn_submit" value="Zum Newsletter anmelden">
                        </div>
                        <p>
                            <?php
                                if ($is_invalid) {
                                    echo "Ungültige Eingaben!";
                                } else {
                                    echo "Erfolgreich gespeichert!";
                                }
                            ?>
                        </p>
                    </fieldset>
                </form>
                <h2 id="wichtig">Das ist uns wichtig</h2>            
                <div>
                    <ul>
                        <li>Beste frische saisonale Zutaten</li>
                        <li>Ausgewogene abwechslungsreiche Gerichte</li>
                        <li>Sauberkeit</li>
                    </ul>
                </div>
                <h2>Wir freuen uns auf Ihren Besuch!</h2>
            </div>
        </div>

    <footer>
        <div>
            <ul>
                <li>(c) E-Mensa GmbH</li>
                <li>Tam Nguyen</li>
                <li><a href="#impressum">Impressum</a></li>
            </ul>
        </div>
    </footer>

    </body>

</html>