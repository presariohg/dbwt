<?php
/**
 * Praktikum DBWT. Autoren:
 * Nguyen, Duc Tam, 3233521
 * Tran, Anh Minh, 3246773
 */
$anweisung = 1;

switch ($anweisung) {
    case true:
        echo "catch _1_ !";
    case 2:
        echo "catch _2_ !";
        break;
    case "1":
        echo "catch _3_ !";
    default:
        echo "nothing to catch!";
        break;
}