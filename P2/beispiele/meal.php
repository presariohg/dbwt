<?php
/**
 * Praktikum DBWT. Autoren:
 * Nguyen, Duc Tam, 3233521
 * Tran, Anh Minh, 3246773
 */
const GET_PARAM_MIN_STARS = 'search_min_stars';
const GET_PARAM_SEARCH_TEXT = 'search_text';
const GET_PARAM_SHOW_DESCRIPTION = 'show_description';
const GET_PARAM_LANGUAGE = 'language';

/**
 * Liste aller möglichen Allergene.
 */
$allergens = [
        'de' => array(11 => 'Gluten',
                      12 => 'Krebstiere',
                      13 => 'Eier',
                      14 => 'Fisch',
                      17 => 'Milch'),
        'en' => array(11 => 'Gluten',
                      12 => 'Crustaceans',
                      13 => 'Eggs',
                      14 => 'Fish',
                      15 => 'Milk')];

$meal = [
        'de' => [ // Kurzschreibweise für ein Array (entspricht = array())
                'name'          => 'Süßkartoffeltaschen mit Frischkäse und Kräutern gefüllt',
                'description'   => 'Die Süßkartoffeln werden vorsichtig aufgeschnitten und der Frischkäse eingefüllt.',
                'price_intern'  => 2.90,
                'price_extern'  => 3.90,
                'allergens'     => [11, 13],
                'amount'        => 42],   // Anzahl der verfügbaren Gerichte.
        'en' => [
                'name'          => 'Sweet potato pockets filled with cream cheese and herbs',
                'description'   => 'The sweet potatoes are carefully cut open and the cream cheese is poured in.',
                'price_intern'  => 2.90,
                'price_extern'  => 3.90,
                'allergens'     => [11,13],
                'amount'        => 42]
];

$static_text = [
        'de' => [
            'gericht'   => 'Gericht',
            'bewertung' => 'Bewertungen',
            'autor'     => 'Autor',
            'sterne'    => 'Sterne',
            'allergen'  => 'Allergene',
            'insgesamt' => 'Insgesamt',
            'suchen'    => 'Suchen'
        ],
        'en' => [
            'gericht'   => 'Dish',
            'bewertung' => 'Reviews',
            'autor'     => 'Author',
            'sterne'    => 'Stars',
            'allergen'  => 'Allergens',
            'insgesamt' => 'Total',
            'suchen'    => 'Search'
        ]];

$ratings = [
    [   'text' => 'Die Kartoffel ist einfach klasse. Nur die Fischstäbchen schmecken nach Käse. ',
        'author' => 'Ute U.',
        'stars' => 2 ],
    [   'text' => 'Sehr gut. Immer wieder gerne',
        'author' => 'Gustav G.',
        'stars' => 4 ],
    [   'text' => 'Der Klassiker für den Wochenstart. Frisch wie immer',
        'author' => 'Renate R.',
        'stars' => 4 ],
    [   'text' => 'Kartoffel ist gut. Das Grüne ist mir suspekt.',
        'author' => 'Marta M.',
        'stars' => 3 ]
];

$language = $_GET[GET_PARAM_LANGUAGE];

// set default language to de
if ($language != 'en')
    $language = 'de';


$included_allergens = [];

// add translated allergens to allergen list
foreach ($meal[$language]['allergens'] as $allergen_index)
    array_push($included_allergens, $allergens[$language][$allergen_index]);


$showRatings = [];
if (!empty($_GET[GET_PARAM_SEARCH_TEXT])) {
    $searchTerm = strtolower($_GET[GET_PARAM_SEARCH_TEXT]);
    foreach ($ratings as $rating) {
        if (strpos(strtolower($rating['text']), $searchTerm) !== false) {
            $showRatings[] = $rating;
        }
    }
} else if (!empty($_GET[GET_PARAM_MIN_STARS])) {
    $minStars = $_GET[GET_PARAM_MIN_STARS];
    foreach ($ratings as $rating) {
        if ($rating['stars'] >= $minStars) {
            $showRatings[] = $rating;
        }
    }
} else {
    $showRatings = $ratings;
}

$is_show_description = ($_GET[GET_PARAM_SHOW_DESCRIPTION] != "off");

function calcMeanStars($ratings) : float { // : float gibt an, dass der Rückgabewert vom Typ "float" ist
    $sum = 0;
    foreach ($ratings as $rating) {
        $sum += $rating['stars'];
    }
    return $sum / count($ratings);
}

?>

<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="UTF-8"/>
        <title>
            <?php echo $static_text[$language]['gericht'].": ".$meal[$language]['name'];?>
        </title>
        <style type="text/css">
            * {
                font-family: Arial, serif;
            }
            .rating {
                color: darkgray;
            }
        </style>
    </head>
    <body>
        <h1>
            <?php echo $static_text[$language]['gericht'].": ".$meal[$language]['name'];?>
        </h1>

        <p>
            <?php
                if ($is_show_description)
                    echo $meal[$language]['description'].'<br>';

                $price_intern = $meal[$language]['price_intern'];
                $price_extern = $meal[$language]['price_extern'];

                echo number_format($price_intern, 2, ',', '').'€ (intern), ';
                echo number_format($price_extern, 2, ',', '').'€ (extern).';
            ?>
        </p>

        <h1>
            <?php
                echo "{$static_text[$language]['bewertung']} ({$static_text[$language]['insgesamt']}: "
                    .calcMeanStars($ratings).")";
            ?>
        </h1>

        <form method="get">
            <label for="search_text">Filter:</label>
            <input id="search_text" type="text" name="search_text"
                   value="<?php echo $_GET[GET_PARAM_SEARCH_TEXT]?>">
            <input type="hidden" name = "language"
                   value="<?php echo $language?>">
            <input type="submit"
                   value="<?php echo $static_text[$language]['suchen']?>">
<!--            <label for="show_description">Beschreibung anzeigen</label>-->
<!--            <input id = "show_description" type="radio" name="show_description">-->
        </form>

        <table class="rating">
            <thead>
            <tr>
                <td><?php echo $static_text[$language]['autor']?></td>
                <td>Text</td>
                <td><?php echo $static_text[$language]['sterne']?></td>
            </tr>
            </thead>
            <tbody>
            <?php
                foreach ($showRatings as $rating) {
                    echo "<tr><td class='author'>{$rating['author']}</td>
                              <td class='rating_text'>{$rating['text']}</td>
                              <td class='rating_stars'>{$rating['stars']}</td>
                          </tr>";
                }
            ?>
            </tbody>
        </table>
        <div>
            <h2><?php echo $static_text[$language]['allergen']?></h2>
            <ul>
                <?php
                    foreach ($included_allergens as $allergen) {
                        echo "<li>{$allergen}</li>";
                    }
                ?>
            </ul>
        </div>
        <footer>
            <a href="<?php
                $query_with_lang = $_GET;
                $query_with_lang['language'] = 'en';
                echo "./meal.php?".http_build_query($query_with_lang)
            ?>">en</a>

            <a href="<?php
                $query_with_lang = $_GET;
                $query_with_lang['language'] = 'de';
                echo "./meal.php?".http_build_query($query_with_lang)
            ?>">de</a>
        </footer>
    </body>
</html>