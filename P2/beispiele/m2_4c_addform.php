<?php
$summand1 = $_POST['summand1'] ?? 0;
$summand2 = $_POST['summand2'] ?? 0;

$sum = $summand1 + $summand2;

$factor1 = $_POST['factor1'] ?? 1;
$factor2 = $_POST['factor2'] ?? 1;

$product = $factor1 * $factor2;
?>

<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="UTF-8">
        <title>P2A5c</title>
    </head>

    <body>
        <div>
            <form action="m2_4c_addform.php" method="post">
                <legend>Addieren</legend>
                <div>
                    <input type="text" name="summand1" id="summand1"
                        value="<?php echo $summand1?>"> +
                    <input type="text" name="summand2" id="summand2"
                        value="<?php echo $summand2?>"> =
                    <?php echo $sum?>
                </div>
                <input type="submit" value="Addieren">
            </form>
        </div>

        <div>
            <form action="m2_4c_addform.php" method="post">
                <legend>Mutiplikation</legend>
                <div>
                    <input type="text" name="factor1" id="factor1"
                           value="<?php echo $factor1?>"> x
                    <input type="text" name="factor2" id="factor2"
                           value="<?php echo $factor2?>"> =
                    <?php echo $product?>
                </div>
                <input type="submit" value="Multiplizieren">
            </form>
        </div>
    </body>
</html>
