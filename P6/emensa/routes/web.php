<?php

use App\Http\Controllers\Controller;
use App\Http\Controllers\DemoController;
use App\Http\Controllers\FeedbackController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/demo', [DemoController::class, 'demo']);
Route::get('/', [Controller::class, 'index']);
Route::get('/anmeldung', [UserController::class, 'login']);
Route::post('/anmeldung_verfizieren', [UserController::class, 'verify']);
Route::get('/abmeldung', [UserController::class, 'logout']);

Route::get('/bewertung', [FeedbackController::class, 'show']);
Route::put('/bewertung', [FeedbackController::class, 'store']);
Route::get('/bewertungen', [FeedbackController::class, 'index']);
Route::get('/meinebewertungen', [FeedbackController::class, 'showUser']);
Route::get('/delete_feedback', [FeedbackController::class, 'delete']);
Route::get('/recommend', [FeedbackController::class, 'recommend']);
