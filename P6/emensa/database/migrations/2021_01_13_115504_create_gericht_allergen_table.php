<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGerichtAllergenTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('gericht_hat_allergen', function (Blueprint $table) {
            $table->string('code')->index();
            $table->unsignedBigInteger('gericht_id')->index();
            $table->foreign('code')->references('code')->on('allergen')
                ->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreign('gericht_id')->references('id')->on('gericht')
                ->cascadeOnDelete()->cascadeOnUpdate();
            $table->primary(["code", "gericht_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('gericht_hat_allergen');
    }
}
