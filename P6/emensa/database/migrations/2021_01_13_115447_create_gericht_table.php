<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateGerichtTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('gericht', function (Blueprint $table) {
            $table->id();
            $table->string('name')->index()->unique();
            $table->text('beschreibung');
            $table->dateTime('erfasst_am')->default(Carbon::now());
            $table->boolean('vegan')->default(false);
            $table->boolean('vegetarisch')->default(false);
            $table->double('preis_intern');
            $table->double('preis_extern');
            $table->string('bildname')->nullable()->default(null);
        });

        DB::statement("ALTER TABLE gericht ADD CONSTRAINT chk_preis CHECK(preis_intern < preis_extern);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('gericht');
    }
}
