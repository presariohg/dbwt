<?php

namespace Database\Seeders;

use App\Models\Counter;
use App\Models\Gericht;
use App\Models\Newsletter;
use Illuminate\Database\Seeder;

class CounterSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $counters = [
            [
                'name' => 'gericht',
                'value' => Gericht::all()->count()
            ],[
                'name' => 'newsletter',
                'value' => Newsletter::all()->count()
            ],[
                'name' => 'visits',
                'value' => 15
            ]
        ];

        foreach ($counters as $counter)
            Counter::create($counter);
    }
}
