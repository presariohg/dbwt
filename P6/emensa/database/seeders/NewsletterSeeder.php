<?php

namespace Database\Seeders;

use App\Models\Newsletter;
use Illuminate\Database\Seeder;

class NewsletterSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Newsletter::create([
           'email' => 'admin@emensa.example',
           'name' => 'admin',
           'language' => 'Deutsch'
        ]);
    }
}
