<?php

namespace Database\Seeders;

use App\Models\GerichtKategorie;
use Illuminate\Database\Seeder;

class GerichtKategorieSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $relations = [
            [
                'kategorie_id' => 3,
                'gericht_id' => 1,
            ],[
                'kategorie_id' => 3,
                'gericht_id' => 3,
            ],[
                'kategorie_id' => 3,
                'gericht_id' => 4,
            ],[
                'kategorie_id' => 3,
                'gericht_id' => 5,
            ],[
                'kategorie_id' => 3,
                'gericht_id' => 6,
            ],[
                'kategorie_id' => 3,
                'gericht_id' => 7,
            ],[
                'kategorie_id' => 3,
                'gericht_id' => 9,
            ],[
                'kategorie_id' => 4,
                'gericht_id' => 16,
            ],[
                'kategorie_id' => 4,
                'gericht_id' => 17,
            ],[
                'kategorie_id' => 4,
                'gericht_id' => 18,
            ],[
                'kategorie_id' => 5,
                'gericht_id' => 16,
            ],[
                'kategorie_id' => 5,
                'gericht_id' => 17,
            ],[
                'kategorie_id' => 5,
                'gericht_id' => 18,
            ],
        ];

        foreach ($relations as $relation)
            GerichtKategorie::create($relation);
    }
}
