<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        $this->call([
            AllergenSeeder::class,
            BenutzerSeeder::class,
            BewertungSeeder::class,
            GerichtSeeder::class,
            KategorieSeeder::class,
            NewsletterSeeder::class,
            CounterSeeder::class,
            GerichtAllergenSeeder::class,
            GerichtKategorieSeeder::class,
        ]);
    }
}
