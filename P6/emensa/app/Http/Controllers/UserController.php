<?php

namespace App\Http\Controllers;


use \Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

$SALT = 'vtv3';

class UserController extends Controller {
    public function login() {
        return view('anmeldung');
    }

    public function verify(Request $request) {

        if (Auth::attempt(['email'=>$request['email'], 'password' => $request['password']], true)) {
            session()->put('user', Auth::user());
            session()->save();
            return redirect('/');
        } else
            return redirect('/anmeldung')->with(['$failed_login' => true]);
    }

    public function logout() {
        session()->flush();
        return redirect('/');
    }
}
