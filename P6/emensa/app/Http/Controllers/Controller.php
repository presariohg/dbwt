<?php

namespace App\Http\Controllers;

use App\Models\Counter;
use App\Models\Gericht;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController {
    public function index(Request $request) {

        $visit_counter = Counter::firstWhere('name', 'visits');
        $visit_counter['value'] += 1;
        $visit_counter->save();

        $counters = [
            'gericht' => Counter::firstWhere('name', 'gericht')['value'],
            'newsletter' => Counter::firstWhere('name', 'newsletter')['value'],
            'visits' => $visit_counter['value'],
        ];

        return view('index')->with([
            'gerichte' => Gericht::all()->take(5),
            'counters' => $counters
        ]);
    }



}
