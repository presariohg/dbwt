<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DemoController extends Controller {
    public function demo(Request $rd) {
//        $vars = [
//            'bgcolor' => $rd->query['bgcolor'] ?? 'ffffff',
//            'name' => $rd->query['name'] ?? 'Dich',
//            'rd' => $rd
//        ];
        return view('demo.demo')->with([
            'bgcolor' => $rd['bgcolor'],
            'name' => $rd['name'],
        ]);
    }
}
