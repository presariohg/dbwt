<?php

namespace App\Http\Controllers;

use App\Models\Bewertung;
use App\Models\Gericht;
use Illuminate\Http\Request;

class FeedbackController extends Controller {
    public function show(Request $request) {
        if (session()->get('user') == null)
            return redirect('anmeldung');


        $gericht_id = $request['gerichtid'];

        $requested_gericht = Gericht::all()->firstWhere('id', $gericht_id);
        return view('bewertung_gericht')->with([
            'gericht' => $requested_gericht
        ]);
    }


    public function showUser(Request $request) {
        $user_id = session()->get('user')->user_id;
        return view('bewertung_index')->with([
            'bewertungen' => Bewertung::all()->
                where('author_id', $user_id)->sortBy('created_at'),

            'is_private' => true,
        ]);
    }


    public function recommend(Request $request) {
        $user = session()->get('user');
        $feedback_id = $request['id'];
        $status = $request['status'];

        if ($user->admin == false ||
            $feedback_id == null ||
            $status == null)
            return redirect('/bewertungen');

        $feedback = Bewertung::all()->firstWhere('id', $feedback_id);
        if ($status == 'on')
            $feedback['is_recommended'] = true;
        elseif ($status == 'off')
            $feedback['is_recommended'] = false;
        $feedback->save();

        return redirect('/bewertungen');

    }


    public function delete(Request $request) {
        $user = session()->get('user');
        $feedback_id = $request['id'];

        if ($user == null ||
            $feedback_id == null)
            return redirect('/meinebewertungen');

        $feedback = Bewertung::all()->firstWhere('id', $feedback_id);
        if ($feedback == null ||
            $feedback->author->user_id != $user->user_id)
            return redirect('/meinebewertungen');

        $feedback->forceDelete();
        return redirect('/meinebewertungen');


    }


    public function index(Request $request) {
        return view('bewertung_index')->with([
            'bewertungen' => Bewertung::all()->take(30)
                ->sortBy('created_at')]);
    }


    public function store(Request $request) {
        if (session()->get('user') == null)
            return redirect('anmeldung');

        if (strlen(trim($request['content'])) <= 5)
            return redirect($request->fullUrl())->with(['invalid_input' => true]);

        Bewertung::create([
            'author_id' => session()->get('user')->user_id,
            'gericht_id' => $request['gericht_id'],
            'content' => $request['content'],
            'stars' => $request['stars']
        ]);
        return redirect($request->fullUrl());
    }
}
