<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Benutzer extends Authenticatable {
    use HasFactory;

    protected $primaryKey = 'user_id';
    protected $table = 'benutzer';

    protected $fillable = [
        'email',
        'password',
        'admin',
        'anzahlfehler',
        'anzahlanmeldungen',
        'letzteranmeldung',
        'letzterfehler'
    ];

    public $timestamps = false;

    public function getAuthIdentifierName() {
        return $this->email;
    }
}
