<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GerichtKategorie extends Model {
    use HasFactory;

    protected $table = 'gericht_hat_kategorie';
    public $incrementing = false;

    protected $fillable = [
        'kategorie_id',
        'gericht_id',
    ];

    public $timestamps = false;
}
