<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GerichtAllergen extends Model {
    use HasFactory;

    protected $table = 'gericht_hat_allergen';
    public $incrementing = false;

    protected $fillable = [
        'code',
        'gericht_id',
    ];

    public $timestamps = false;
}
