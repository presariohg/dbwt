<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bewertung extends Model {
    use HasFactory;

    protected $table = 'bewertung';
    protected $primaryKey = 'id';

    protected $fillable = [
        'author_id',
        'gericht_id',
        'stars',
        'content'
    ];

    public function author() {
        return $this->belongsTo(Benutzer::class, 'author_id', 'user_id');
    }

    public function gericht() {
        return $this->belongsTo(Gericht::class, 'gericht_id', 'id');
    }

    public function rating() {
        switch ($this->stars) {
            case 0:
                return 'Sehr Schlect';
            case 1:
                return 'Schlect';
            case 2:
                return 'Gut';
            default:
                return 'Sehr Gut';
        }
    }
}
