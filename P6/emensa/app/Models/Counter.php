<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Counter extends Model {
    use HasFactory;

    protected $primaryKey = 'name';
    protected $table = 'counters';

    protected $fillable = [
        'name',
        'value'
    ];

    public $incrementing = false;
    public $timestamps = false;
}
