@extends('emensa.emensa_layout')

@section('content')
    <div style="border: thin solid black; padding: 5px 10px 5px 10px">
        <h2>Anmeldung</h2>

        @if(isset($failed_login) && $failed_login == false)
            <p style="color: red">Email oder Passwort ist falsch</p>
        @endif
        <hr>

        <form action="/anmeldung_verfizieren" method="post" accept-charset="UTF-8">
            @csrf
            @method('POST')
            <input type="text" name="email" placeholder="Email" required>
            <br><br>
            <input type="password" name="password" placeholder="Passwort" required>
            <br><br>
            <input type="submit" value="Anmeldung">
        </form>
    </div>
@endsection
