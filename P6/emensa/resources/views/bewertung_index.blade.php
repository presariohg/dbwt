@extends('emensa.emensa_layout')

@section('content')
    <div style="border: thin solid black; padding: 5px 10px 5px 10px;
            width: 50%; height: 100%;">
        @foreach($bewertungen as $bewertung)
            <div>
                @if($bewertung->is_recommended)
                    <p style="font-weight: bold">
                @else
                    <p>
                @endif
                    Benutzer {{$bewertung->author->email}} hat am {{$bewertung->created_at}} geschrieben:<br>
                    {{$bewertung->content}}<br>
                    für {{$bewertung->gericht->name}}<br>
                    rating: {{$bewertung->rating()}}
                </p><br>
                @if(isset($is_private) && $is_private == true)
                    <a href="/delete_feedback?id={{$bewertung->id}}">Löschen</a>
                @endif
                @if(session()->get('user')->admin == true)
                    @if(!$bewertung->is_recommended)
                        <a href="/recommend?id={{$bewertung->id}}&status=on">Hervorheben</a>
                    @else
                        <a href="/recommend?id={{$bewertung->id}}&status=off">Hervorhebung abwählen</a>
                    @endif
                @endif
            </div><hr>
        @endforeach
    </div>
@endsection
