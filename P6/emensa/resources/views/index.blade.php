@extends('emensa.emensa_layout')

@section('content')
    @if(session()->get('user') !== null)
        <p>Angemeldet als {{session()->get('user')->email}}</p>
    @endif
    <img src="banner.jpg" alt="Something banner something" id="img_banner">

    <h2>Bald gibt es Essen auch online ;-)</h2>
    <p id="textbox">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br>
        Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt.
    </p>

    <h2>Köstlichkeiten, die Sie erwarten</h2>
    <table id = "tbl_menu">
        <thead>
        <tr>
            <th>Bild</th>
            <th>Speise</th>
            <th>Preis intern</th>
            <th>Preis extern</th>
            <th>Vegan</th>
            <th>Vegetarisch</th>
            <th>Allergene</th>
        </tr>
        </thead>
        @foreach($gerichte as $gericht)
            <tr>
                <td style="margin: 0 2px 0 2px;">
                    <img src = "img/gerichte/{{$gericht['bildname'] ?? '00_image_missing.jpg'}}"
                         alt="img place holder" width="150" height="150">
                </td>
                <td>
                    @if(session()->get('user') !== null)
                        <a href="/bewertung?gerichtid={{$gericht['id']}}">
                            {{$gericht['name']}}
                        </a>
                    @else
                        {{$gericht['name']}}
                    @endif
                </td>
                <td>{{$gericht->preis_intern}}</td>
                <td>{{$gericht->preis_extern}}</td>
                <td>{{$gericht->vegan}}</td>
                <td>{{$gericht->vegetarisch}}</td>
                <td>
                    @foreach($gericht->allergens as $allergen)
                        {{$allergen->name}}
                    @endforeach
                </td>
            </tr>
        @endforeach
    </table>

    <h2>E-Mensa in Zahlen</h2>

    <div id="div_mid_list">
        <ol>
            <li>{{$counters['visits']}} Besuche</li>

            <li>{{$counters['newsletter']}} Anmeldungen zum Newsletter</li>

            <li>{{$counters['gericht']}} Speisen</li>
        </ol>
    </div>

    <h2>Interesse geweckt? Wir informieren Sie!</h2>
    <form action="index.php" method="post" accept-charset="utf-8">
        <fieldset id="fs_newsletter" >
            <div id="div_vorname">
                <label for="vorname">Ihr Name:</label>
                <input required id="vorname" name="vorname" placeholder="Vorname">
            </div>
            <div id="div_email">
                <label for="email">Ihre E-Mail:</label>
                <input required id="email" name="email" placeholder="admin@example.com">
            </div>
            <div id="div_language">
                <label for="language">Newsletter bitte in:</label>
                <select id="language" name="language">
                    <option>Deutsch</option>
                    <option>Englisch</option>
                </select>
            </div>
            <div id="div_confirm_read">
                <input required id="confirm_read" type="checkbox" name="confirm_read">
                <label for="confirm_read">Den Datenschutzbestimmungen stimme ich zu</label>
            </div>
            <div id="div_submit">
                <input type="submit" name="btn_submit" value="Zum Newsletter anmelden">
            </div>

            @if (isset($has_request))
                <p>
                    @if ($is_valid_registration)
                        "Erfolgreich gespeichert!"
                    @else
                        "Ungültige Eingaben!"
                    @endif
                </p>
            @endif
        </fieldset>
    </form>
    <h2 id="wichtig">Das ist uns wichtig</h2>
    <div>
        <ul>
            <li>Beste frische saisonale Zutaten</li>
            <li>Ausgewogene abwechslungsreiche Gerichte</li>
            <li>Sauberkeit</li>
        </ul>
    </div>
    <a id="wunschgericht" href="wunschgericht.php">Ihr Wunschgericht hier erstellen</a>
    <h2>Wir freuen uns auf Ihren Besuch!</h2>
@endsection
