<!DOCTYPE html>
<html lang="de">
    <head>
        <title>@yield('title')</title>
        <style type="text/css">
            body {
                background-color: {{$rd->query['bgcolor'] ?? 'ffffff'}};
                font-family: "Arial", serif;
            }

            @yield('style')
        </style>
    </head>

    <body>
        <header>
            @yield('header')
        </header>

        @yield('content')

        <footer>
            @yield('footer')<br>
            &copy; B22 DBWT
        </footer>
    </body>
</html>