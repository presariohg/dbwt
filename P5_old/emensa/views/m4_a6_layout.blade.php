<!DOCTYPE html>
<html lang="de">
    <head>
        <title>@yield('title')</title>
        <style type="text/css">
            body {
                background-color: {{$rd->query['bgcolor'] ?? 'ffffff'}};
                font-family: "Arial", serif;
            }

            @yield('style')
        </style>
    </head>

    <body>
        <header>
            <h1>@yield('title')</h1>
        </header>

        @yield('content')

        <footer>
            &copy; B22 DBWT
        </footer>
    </body>
</html>