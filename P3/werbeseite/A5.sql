# Praktikum DBWT. Autoren:
#     Nguyen, Duc Tam, 3233521
#     Tran, Anh Minh, 3246773
#

use werbeseiteemensa;

# 5.1
select * from gericht;

# 5.2
select erfasst_am from gericht;

# 5.3
select name as Gerichname, erfasst_am from gericht order by name desc;

# 5.4
select name, beschreibung from gericht order by name limit 5;

# 5.5
select name, beschreibung from gericht order by name limit 10 offset 5;

# 5.6
select distinct typ from allergen;

# 5.7
select name from gericht where name like 'K%';

# 5.8
select id, name from gericht where name like '%suppe%';

# 5.9
select * from kategorie where eltern_id is null;

# 5.10
select gericht.name, allergen.name from gericht
    join gericht_hat_allergen on gericht.id = gericht_hat_allergen.gericht_id
    join allergen on allergen.code = gericht_hat_allergen.code;

# 5.11
select gericht.name, allergen.name from gericht
    left join gericht_hat_allergen on gericht.id = gericht_hat_allergen.gericht_id
    left join allergen on allergen.code = gericht_hat_allergen.code;

select gericht.name, allergen.name from gericht
    left join gericht_hat_allergen on gericht.id = gericht_hat_allergen.gericht_id
    left join allergen on allergen.code = gericht_hat_allergen.code
    order by gericht.name limit 5;

# 5.12
select gericht.name, allergen.name from gericht
    join gericht_hat_allergen on gericht.id = gericht_hat_allergen.gericht_id
    right join allergen on allergen.code = gericht_hat_allergen.code;

# 5.13
select kategorie.name, count(gericht.name) as Anzahl from gericht
    join gericht_hat_kategorie on gericht.id = gericht_hat_kategorie.gericht_id
    join kategorie on gericht_hat_kategorie.kategorie_id = kategorie.id
    group by kategorie.name order by Anzahl;

# 5.14
select kategorie.name, count(gericht.name) as Anzahl from gericht
    join gericht_hat_kategorie on gericht.id = gericht_hat_kategorie.gericht_id
    join kategorie on gericht_hat_kategorie.kategorie_id = kategorie.id
    group by kategorie.name having Anzahl > 2 order by Anzahl;

# 5.15
update allergen set name = 'Kamut' where code = 'a6';

# 5.16
insert into gericht
    values (21, 'Currywurst mit Pommes', 'Currywurst beschreibung', '2020-11-19', false, false, 1.2, 2.3);
insert into gericht_hat_kategorie
    values (3, 21);